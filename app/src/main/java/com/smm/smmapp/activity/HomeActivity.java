package com.smm.smmapp.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.widget.RadioGroup;

import com.facebook.login.LoginManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.smm.smmapp.R;
import com.smm.smmapp.databinding.ActivityHomeBinding;
import com.smm.smmapp.fragment.HomeFragment;
import com.smm.smmapp.fragment.PublisherFragment;
import com.smm.smmapp.fragment.TimelineFragment;

/**
 * Created by Nickolay on 22.06.2017.
 */

public class HomeActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    ActivityHomeBinding mBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        mBinding.logoutButton.setOnClickListener(view -> logout());
        mBinding.toolbar.radioGroupMenu.setOnCheckedChangeListener(mToolbarItemCheckedListener);
        init();
    }

    private void init(){
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, new HomeFragment())
                .commit();
    }

    RadioGroup.OnCheckedChangeListener mToolbarItemCheckedListener = (radioGroup, id) -> {
        Fragment fragment = null;
        switch (id) {
            case R.id.radioHome:
                fragment = new HomeFragment();
                break;
            case R.id.radioFeed:
                fragment = new TimelineFragment();
                break;
            case R.id.radioPublisher:
                fragment = new PublisherFragment();
                break;
        }
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    private void logout() {
        LoginManager.getInstance().logOut();
        finish();
    }
}
