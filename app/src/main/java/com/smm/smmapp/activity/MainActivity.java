package com.smm.smmapp.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.smm.smmapp.R;
import com.smm.smmapp.fragment.SignInLoginFragment;
import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;

public class MainActivity extends AppCompatActivity {

    public static final String TWITTER_KEY = "UwemsrEWyuxtm7fJecwXQuDSn";
    public static final String TWITTER_SECRET = "nrHrMyLuDl27mD1dN9bkdbDypOIh9IsxHiQK1HlgIQxdsmTM1w";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TwitterConfig config = new TwitterConfig.Builder(this)
                .logger(new DefaultLogger(Log.DEBUG))
                .twitterAuthConfig(new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET))
                .debug(true)
                .build();
        Twitter.initialize(config);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, new SignInLoginFragment())
                .commit();
    }
}
