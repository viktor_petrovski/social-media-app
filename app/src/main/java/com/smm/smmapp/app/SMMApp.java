package com.smm.smmapp.app;

import android.app.Application;

import com.facebook.appevents.AppEventsLogger;
import com.smm.smmapp.R;

/**
 * Created by Anatol Salanevich on 19.06.2017.
 */

public class SMMApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        AppEventsLogger.activateApp(this, getString(R.string.facebook_app_id));
    }
}
