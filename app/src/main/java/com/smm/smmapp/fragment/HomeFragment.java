package com.smm.smmapp.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.smm.smmapp.R;
import com.smm.smmapp.databinding.FragmentHomeBinding;
import com.smm.smmapp.databinding.InstagramAuthActivityBinding;
import com.smm.smmapp.utils.AppHelper;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;

/**
 * Created by Nickolay on 22.06.2017.
 */

public class HomeFragment extends Fragment {

    FragmentHomeBinding mBinding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mBinding = FragmentHomeBinding.inflate(inflater, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mBinding.loginButton.setCallback(mTwitterSessionCallback);

        mBinding.twitterSwitch.setOnCheckedChangeListener((compoundButton, b) -> mBinding.loginButton.callOnClick());
        //TODO connect
        //mBinding.instagramSwitch.setOnCheckedChangeListener((compoundButton, b) -> startActivity(new Intent(getContext(), InstagramAuthActivity.class)));
        mBinding.youtubeSwitch.setOnCheckedChangeListener((compoundButton, b) -> mBinding.youtubeSwitch.callOnClick());

        mBinding.instagramButton.setOnClickListener(mViewsClickListener);
        mBinding.facebookButton.setOnClickListener(mViewsClickListener);
        mBinding.snapchatButton.setOnClickListener(mViewsClickListener);
        mBinding.whatsappButton.setOnClickListener(mViewsClickListener);
    }

    View.OnClickListener mViewsClickListener = view -> {
        switch (view.getId()) {
            case R.id.whatsapp_button:
                if (AppHelper.isWhatsAppInstalled(getContext())) {
                    startActivity(getActivity().getPackageManager().getLaunchIntentForPackage("com.whatsapp"));
                    return;
                }
                Toast.makeText(getContext(), "WhatsApp is not installed", Toast.LENGTH_SHORT).show();
                break;
            case R.id.facebook_button:
                if (AppHelper.isFacebookInstalled(getContext())) {
                    String uri = "facebook://facebook.com/inbox";
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    startActivity(intent);
                    return;
                }
                Toast.makeText(getContext(), "Facebook is not installed", Toast.LENGTH_SHORT).show();
                break;
            case R.id.snapchat_button:
                if (AppHelper.isSnapchatInstalled(getContext())) {
                    startActivity(getActivity().getPackageManager().getLaunchIntentForPackage("com.snapchat.android"));
                    return;
                }
                Toast.makeText(getContext(), "Snapchat is not installed", Toast.LENGTH_SHORT).show();
                break;
            case R.id.instagram_button:
                if (AppHelper.isInstagramInstalled(getContext())) {
                    startActivity(getActivity().getPackageManager().getLaunchIntentForPackage("com.instagram.android"));
                    return;
                }
                Toast.makeText(getContext(), "Instagram is not installed", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getContext(), InstagramAuthActivityBinding.class));

        }
    };



    Callback<TwitterSession> mTwitterSessionCallback = new Callback<TwitterSession>() {
        @Override
        public void success(Result<TwitterSession> result) {

        }

        @Override
        public void failure(TwitterException exception) {
            mBinding.twitterSwitch.setChecked(false);
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mBinding.loginButton.onActivityResult(requestCode, resultCode, data);
    }
}
