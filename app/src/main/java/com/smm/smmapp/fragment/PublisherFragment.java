package com.smm.smmapp.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.smm.smmapp.databinding.FragmentPublisherBinding;
import com.smm.smmapp.utils.AppHelper;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

/**
 * Created by Nickolay on 22.06.2017.
 */

public class PublisherFragment extends Fragment {

    FragmentPublisherBinding mBinding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentPublisherBinding.inflate(inflater, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mBinding.whatsappPublisherButton.setOnClickListener(btnView -> {
            if (!AppHelper.isWhatsAppInstalled(getContext())) {
                Toast.makeText(getContext(), "WhatsApp is not installed", Toast.LENGTH_SHORT).show();
                return;
            }
            String uri = "whatsapp://send?text=" + mBinding.textField.getText().toString();
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(uri)));
        });

        mBinding.twitterPublisherButton.setOnClickListener(btnView -> new TweetComposer
                .Builder(getContext())
                .text(mBinding.textField.getText().toString())
                .show());
    }
}
