package com.smm.smmapp.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.smm.smmapp.R;
import com.smm.smmapp.activity.HomeActivity;
import com.smm.smmapp.databinding.FragmentSigninSignupBinding;

public class SignInLoginFragment extends Fragment implements View.OnClickListener {

    CallbackManager mCallbackManager;
    FragmentSigninSignupBinding mBinding;

    private static final String FB_READ_EMAIL_PERM = "email";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCallbackManager = CallbackManager.Factory.create();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentSigninSignupBinding.inflate(inflater, container, false);
        mBinding.loginButton.setReadPermissions(FB_READ_EMAIL_PERM);
        mBinding.loginButton.setFragment(this);
        mBinding.loginButton.registerCallback(mCallbackManager, mFacebookCallback);
        return mBinding.getRoot();
    }

    FacebookCallback mFacebookCallback = new FacebookCallback() {
        @Override
        public void onSuccess(Object o) {
            startActivity(new Intent(getContext(), HomeActivity.class));
        }

        @Override
        public void onCancel() {
            Toast.makeText(getContext(), R.string.operation_was_canceled, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onError(FacebookException error) {
            Toast.makeText(getContext(), R.string.error, Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBinding.signIn.setOnClickListener(this);
        mBinding.signUp.setOnClickListener(this);
        mBinding.mail.setOnClickListener(this);
        mBinding.register.setOnClickListener(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sign_in:
                signInMode();
                break;
            case R.id.sign_up:
                signUpMode();
                break;
            case R.id.register:
                goNext();
                break;
        }
    }

    private void goNext() {
        String emailValue = mBinding.mailValue.getText().toString();
        if (TextUtils.isEmpty(emailValue)) {
            // TODO use databinding validation
            mBinding.mailValue.setError("Email is empty");
            return;
        }
        String passwordValue = mBinding.passwordValue.getText().toString();
        if (TextUtils.isEmpty(passwordValue)) {
            // TODO use databinding validation
            mBinding.passwordValue.setError("Email is empty");
            return;
        }
        //todo: send credentials to server
        startActivity(new Intent(getContext(), HomeActivity.class));
    }

    private void signUpMode() {
        mBinding.signIn.setTextColor(Color.GRAY);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mBinding.signUp.setTextColor(getResources().getColor(R.color.sign_n_color, null));
        } else {
            mBinding.signUp.setTextColor(getResources().getColor(R.color.sign_n_color));
        }
        mBinding.buttonText.setText(R.string.register);
    }

    private void signInMode() {
        mBinding.signUp.setTextColor(Color.GRAY);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mBinding.signIn.setTextColor(getResources().getColor(R.color.sign_n_color, null));
        } else {
            mBinding.signIn.setTextColor(getResources().getColor(R.color.sign_n_color));
        }
        mBinding.buttonText.setText(R.string.log_in);
    }
}
