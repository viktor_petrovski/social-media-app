package com.smm.smmapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.minimize.android.rxrecycleradapter.RxDataSource;
import com.smm.smmapp.R;
import com.smm.smmapp.databinding.FragmentTimelineBinding;
import com.smm.smmapp.databinding.TweetBinding;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.models.Tweet;

import java.util.List;

/**
 * Created by Nickolay on 22.06.2017.
 */

public class TimelineFragment extends Fragment {

    FragmentTimelineBinding mBinding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentTimelineBinding.inflate(inflater, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TwitterCore
                .getInstance()
                .getApiClient()
                .getStatusesService()
                .homeTimeline(200, null, null, null, null, true, false)
                .enqueue(mTwitterCallback);
    }

    Callback<List<Tweet>> mTwitterCallback = new Callback<List<Tweet>>() {
        @Override
        public void success(Result<List<Tweet>> result) {
            mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            RxDataSource<Tweet> dataSource = new RxDataSource<>(result.data);
            dataSource.<TweetBinding>bindRecyclerView(mBinding.recyclerView, R.layout.tweet)
                    .doOnNext(holder -> {
                        holder.getViewDataBinding().setTweet(holder.getItem());
                    })
                    .subscribe();
        }

        @Override
        public void failure(TwitterException exception) {

        }
    };
}
