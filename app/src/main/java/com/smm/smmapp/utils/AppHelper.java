package com.smm.smmapp.utils;

import android.content.Context;
import android.content.pm.PackageManager;

/**
 * Created by Anatol Salanevich on 18.06.2017.
 */

public class AppHelper {

    private AppHelper() {}

    public static boolean isWhatsAppInstalled(Context context) {
        return isAppInstalled("com.whatsapp", context);
    }

    public static boolean isFacebookInstalled(Context context) {
        return isAppInstalled("com.facebook.katana", context);
    }

    public static boolean isSnapchatInstalled(Context context) {
        return isAppInstalled("com.snapchat.android", context);
    }

    public static boolean isInstagramInstalled(Context context) {
        return isAppInstalled("com.instagram.android", context);
    }

    private static boolean isAppInstalled(String uri, Context context) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

}
